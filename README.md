# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Mis sai tehtud? ###

* On võimalus pidada blogi
* On olemas leht, kus saaks enda kohta kirjutada(olemas ka näidis)
* On võimalus teha galeriid piltidest, mida hiljem saab kommenteerida
* Sai installitud arcade-basic teema
* Leht jookseb wordpress viimase versiooni peal
* Kasutan Jetpacki mosaic vaadet galerii jaoks
* On tehtud git repositoorium bitbucketis 
* näidis variant on olemas online: http://photooasis.azurewebsites.net/
* localhosti versioon on repositooriumis olemas. Andmebaas on olemas kaustas: Database_at_the_moment
* Loodud on näidis postitused ja tehtud ka näidis tutvustusleht


### Localhostis installeerimine ###

* Installerida wamp (www.wampserver.com/en/)
* Vaikimisi peaks olema wamp direktoorium: c://wamp/www --> tekitada sinna kaust nimega näiteks 'oasis'
* kasutades sourcetree või mõni muu rakendus laadida alla repositooriumi failid
* minna aadressile localhost ning valida phpmyadmin
* tekitame uue andmebaasi nimega näiteks oasis
* impordime sql faili kaustast Database_at_the_moment
* lähme aadressile localhost/oasis --> peaks ilmuma wordpressi sätted
* Valime nimi ja parooli -->vajutame edasi
* Valime meie loodud andmebaasi --> kasutajaks on root ja parooli jätame tühjaks
* Seejärel peaks localhostis leht ette tulema