<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'oasis');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_b<zz3Ho4KXT+}Rjr>2-BvHeWW~(<DP(ltUT.H{c`p,=LR9En%D43W@r2q*REY}Z');
define('SECURE_AUTH_KEY',  '+lyWu,1$8@{1hzJUsy|a^2O]A?uQXz4x0Y(f_u=8%6ga!3.Dx|ng)r&Oc3TC$Y%!');
define('LOGGED_IN_KEY',    'Stp<K/!EZ}O*hrI8!W}^BW4Z VoJ}lh5I Wp]JT{o>|)+d-p~>1/-B6xVARFTJ0E');
define('NONCE_KEY',        '10{462jSm^E^_W-[:&C)W-)<QxKl+lwIQ%k|RsY=@Po(r,_=7e+z,Of!l3pC(E-@');
define('AUTH_SALT',        'j%hJJ12W}oW?H3K-P|Q%[+}QXB%s|=Y?1*^U]D3:-aC!;&<+K82&!7sNgu#x[AMO');
define('SECURE_AUTH_SALT', ',DZ]%T}-|f]:x1G-+|@ c?8NM!LO|L_2D6QW1op2Q*f_yB!of.TyhS9DpQ``i-c,');
define('LOGGED_IN_SALT',   '#4}8SN-6o/c {$Tq&?/um$_hc~O{Pm7M&o5jR2xN3mYn%_jq8-`7QqsS[myCuiDF');
define('NONCE_SALT',       'GRuhv_g=RL:;>alnfmaQo2+)GbEz=t!2UAcs+LFu^YKRkWq2j|&M+#^T6D16 Ib&');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
